import React, { Suspense, lazy, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import MainLoyaut from './layouts/MainLoyaut';
import LoadingScreen from './components/LoadingSreen/LoadingSreen';

import GuestGuard from './components/Guards/guestGuard';

export const renderRoutes = (routes = []) => (
  <Suspense fallback={<LoadingScreen />}>
    <Switch>
      {routes.map(route => {
        const Guard = route.guard || Fragment;
        const Component = route.component;
        const Layout = route.layout;
        return (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            render={props => {
              return (
                <Guard>
                  <Layout>
                    <Component {...props} />
                  </Layout>
                </Guard>
              );
            }}
          />
        );
      })}
    </Switch>
  </Suspense>
);

const routes = [
  {
    exact: true,
    guard: GuestGuard,
    path: '/login',
    layout: MainLoyaut,
    component: lazy(() => import('./views/login')),
  },

  {
    exact: true,
    guard: GuestGuard,
    path: '/signup',
    layout: MainLoyaut,
    component: lazy(() => import('./views/signup')),
  },

  {
    exact: true,
    path: '/',
    layout: MainLoyaut,
    component: lazy(() => import('./views/MainPage')),
  },
  {
    exact: true,
    path: '/profile/:link',
    layout: MainLoyaut,
    component: lazy(() => import('./views/ProfilePage')),
  },
  {
    exact: true,
    path: '/media',
    layout: MainLoyaut,
    component: lazy(() => import('./views/MediaPage')),
  },

  {
    exact: true,
    path: '/three',
    layout: MainLoyaut,
    component: lazy(() => import('./views/Threejs')),
  },

  {
    exact: true,
    path: '/messenger',
    layout: MainLoyaut,
    component: lazy(() => import('./views/Messenger')),
  },
];

export default routes;
