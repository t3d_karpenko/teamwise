
import axios from '../../utils/axios';
import { url } from '../../config';
import { useEffect, useState } from 'react';
import useAuth from '../../hooks/useAuth';
import useMessenger from '../../hooks/useMesenger';
import { ReactComponent as ArrowDownIcon } from '../../assets/sortDropdown.svg';
import { ReactComponent as RecevidMessages } from '../../assets/RecevidMessages.svg';
import { ReactComponent as SettingIcon } from '../../assets/headerIconSetting.svg';
import { ReactComponent as Attach } from '../../assets/attach.svg';
import { ReactComponent as AvatarUser } from '../../assets/user.svg';
import { ReactComponent as Logo } from '../../assets/logo.svg';
import './style.scss';

const UsersList = ({ toggleModal, setToggleModal, setThread}) => {

  const [users, setUsers] = useState('');

  useEffect(() => {
    axios.get(`${url}/api/messenger/users`).then(res => {
      setUsers(res.data.body);
    });
  }, []);
  if (!toggleModal || !users) {
    return <> </>;
  }

  const newThread = user => {
    setToggleModal(false);
    axios.post(`${url}/api/messenger/newThread`, { user }).then(res => {
      setThread(res.data.body._id);
      
    });
  };

  

  return (
    <>
      <div
        className="wrapperModalMessenger"
        onClick={() => setToggleModal(false)}
      >
        <div className="ModalMessanger">
          {users?.map(item => {
            return (
              <div
                key={item._id}
                onClick={() => {
                  newThread(item._id);
                }}
                className="itemChatList"
              >
                {item.avatar ? (
                  <img src={item.avatar} alt="avatar" />
                ) : (
                  <div className="avatarItemChatList">
                    {' '}
                    <AvatarUser />{' '}
                  </div>
                )}
                <div className="nameItemChatList">
                  {`${item.firstName || ''} ${item.lastName || ''}`}
                </div>
              </div>
            );
          })}
        </div>
      </div>{' '}
    </>
  );
};

const Messenger = () => {

  const { user } = useAuth();
  const { threads, thread, readMessages, setThread  } = useMessenger();


  const [message, setMessage] = useState('');
  const [toggleModal, setToggleModal] = useState(false);
  const [toggleDropdawnHeaderMessages, setToggleDropdawnHeaderMessages] = useState(false);

  // const [thread, setThread] = useState(null);
  
//   const [threads, setThreads] = useState([]);

//   useEffect(() => {
//     if (!user) return;
//     axios.get(`${url}/api/messenger/threads`).then(res => {
//       setThreads(res.data.body);
//     });

//     const socket = io(url, {
//       transports: ['websocket', 'polling', 'flashsocket'],
//       query: `user=${user._id}`,
//     });

//     socket.on('connect', () => {
//       console.log('socket connect');
//     });

//     socket.on('disconnect', reason => {
//       if (reason === 'io server disconnect') {
//         socket.connect();
//       }
//     });

//     socket.on('message', data => {
//       setThreads(prev =>
//         prev.map(item => {
//           if (item._id === data.thread) {
//             return {
//               ...item,
//               messages: [data, ...item.messages],
//             };
//           }
//           return item;
//         }),
//       );
//     });

//     socket.on('thread', data => {
//       setThreads(prev => [data, ...prev]);
//     });

//     return () => {
//       if (socket) {
//         socket.disconnect();
//       }
//     };
//   }, [user]);
  

useEffect(() => {
  return () => {
    setThread(null)
  }
  // eslint-disable-next-line 
},[])



  const submit = async () => {
    await axios.post(`${url}/api/messenger/newMessage/${thread}`, {
      text: message,
    });
    setMessage('');
  };

  const deleteThread = async () => { 
    setThread(null);
    await axios.delete(`${url}/api/messenger/thread/${thread}`).catch(err => {
        console.log(err);   
    })
    
  }
  const getName  = () => {
    const interlocutorName = threads.find(item => item._id === thread)?.members.find(item => item._id !== user._id)
    return `${interlocutorName?.firstName} ${interlocutorName?.lastName}`
  }


  return (
    <>
      <UsersList
        toggleModal={toggleModal}
        setToggleModal={setToggleModal}
        setThread={setThread}
      />
      <div className="wrapperMessenger">
        <div className="containerMessenger">
          <div className="chatListContainer">
            <div
              className="btnOpenModallMessenger"
              onClick={() => setToggleModal(prev => !prev)}
            >
              +
            </div>
            <div className="headerChatList">
              <div className="iconReceivedMessage">
                <RecevidMessages />
              </div>
              <div>Received messages</div>
              <ArrowDownIcon />
            </div>

            <div className="blockChatList">
              {threads?.map(item => {
                const sender = item.members.find(
                  item => item._id !== user?._id,
                );
                return (
                  <div
                    key={item._id}
                   
                    onClick={() => {
                      setThread(item._id);
                      setToggleDropdawnHeaderMessages(false);
                      readMessages(item._id)
                    }}
                    className={
                      item._id === thread
                        ? 'itemChatList active'
                        : 'itemChatList '
                    }
                  >
                    {sender.avatar ? (
                      <img src={sender.avatar} alt="avatar" />
                    ) : (
                      <div className="avatarBlockItemChatList">
                        {' '}
                        <AvatarUser />{' '}
                      </div>
                    )}
                    <div className='infoBlockItemChatList'>
                    <div className="nameItemChatList">
                      {`${sender.firstName || ''} ${sender.lastName || ''}`}
                    </div>
                    <div className="lastMessageItemChatList">
                        {item.messages?.[0]?.text  || 'start chat' }
                    </div>
                    </div>
                    <div className='dateAndQtyBlockItemChatList'>
                          {item?.messages?.[0] && <div className='dateItemChatList'>{new Date(item?.messages?.[0]?.createdAt).toLocaleDateString()}</div>}
                          {item?.messages.filter(item => item.sender._id !== user._id && item.isRead !== true ).length > 0 &&<div className='qtyMessageItemChatList'>{item?.messages.filter(item => item.sender._id !== user._id && item.isRead !== true ).length}</div>}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>

          <div className="messagesContainer">
            {thread ? (
              <>
                <div className="headerMessages">{console.log(1111)}
                  <div>Messages{console.log(2222)}</div>
                  <div className="interlocutorNameHeaderMessages">
                 {getName()}
                  </div>
                  <div className="iconSettingHeaderMessages" onClick={() => setToggleDropdawnHeaderMessages(prev => !prev)}>
                    
                    <SettingIcon />
                    {toggleDropdawnHeaderMessages ? <div className='dropdpwnSettingHeaderMessages' onClick={() => {deleteThread()}}>
                        delete chat
                    </div>: <></>}
                  </div>
                </div>

                <div className="blockMessages">
                  {threads
                    .find(item => item._id === thread)
                    ?.messages.map(item => (
                      <div
                        key={item._id}
                        className={
                          item.sender._id !== user._id
                            ? 'messageBlockMessages'
                            : 'messageBlockMessages myMessage'
                        }
                      >
                        {item.sender.avatar ? (
                          <img src={item.sender.avatar} alt="avatar" />
                        ) : (
                          <div className="avatarItemChatList">
                            {' '}
                            <AvatarUser />{' '}
                          </div>
                        )}

                        <div className="messageText">{item.text}</div>
                      </div>
                    ))}
                </div>

                <div className="blockInputMessages">
                  <textarea
                    value={message}
                    onChange={e => setMessage(e.target.value)}
                    placeholder={`Reply to ${getName()}`}
                    onKeyDown={e => {
                      if (e.keyCode === 13 && e.shiftKey === false) {
                        e.preventDefault();
                        submit();
                      }
                    }}
                  />
                  <div className="iconAttach">
                    <Attach />{' '}
                  </div>
                </div>
              </>
            ) : (
              <div className="logoWindowMessagesContainer">
                <Logo />
              </div>
            )}
          </div>
        </div>
      </div>{' '}
    </>
  );
};

export default Messenger;
