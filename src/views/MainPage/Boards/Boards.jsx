import ItemBoard from './../../../components/ItemBoard';
import ReactDOM from 'react-dom';
import { useState } from 'react';
import ModalAllBoard from '../../../components/ItemBoard/ModalAllBoard';

import './style.scss';

const Boards = ({ moods, qty = 2, userPager }) => {
  const [toggleModalAll, setToggleModalAll] = useState(false);

  return (
    <>
      <div className="wrapperBoards">
        <div className="containerBoards">
          {moods.slice(0, qty).map(item => (
            <div key={item._id} className="wrapperItemBoard">
              {' '}
              <ItemBoard item={item} />{' '}
            </div>
          ))}
          <div
            className="MainBtnMore"
            onClick={() => setToggleModalAll(prev => !prev)}
          >
            More
          </div>
        </div>
      </div>
      {toggleModalAll &&
        ReactDOM.createPortal(
          <ModalAllBoard
            qty={moods.length}
            userPager={userPager}
            userBoard={moods}
            toggleModalAll={toggleModalAll}
            setToggleModalAll={setToggleModalAll}
          />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default Boards;
