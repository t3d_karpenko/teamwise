import ReactDOM from 'react-dom';
import { useState } from 'react';
import ModalAllArticle from './../../../components/ItemArticle/ModalAllArticle';
import ItemArticle from '../../../components/ItemArticle';

import './style.scss';

const Articles = ({ articles, qty = 2, userPager }) => {
  const [toggleModalAll, setToggleModalAll] = useState(false);

  return (
    <>
      <div className="wrapperArticles">
        <div className="containerArticles">
          {articles.slice(0, qty).map(item => (
            <div key={item._id} className="wrapperItemArticle">
              <ItemArticle item={item} />
            </div>
          ))}
          <div
            className="MainBtnMore"
            onClick={() => setToggleModalAll(prev => !prev)}
          >
            More
          </div>
        </div>
      </div>
      {toggleModalAll &&
        ReactDOM.createPortal(
          <ModalAllArticle
            qty={articles.length}
            userPager={userPager}
            userArticles={articles}
            toggleModalAll={toggleModalAll}
            setToggleModalAll={setToggleModalAll}
          />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default Articles;
