import Slider from 'react-slick';
import 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import './style.scss';

const Carousel = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    arrows: false,
    dotsClass: 'slickDots',
    className: 'sliderSlict',

    appendDots: dots => (
      <div style={{}}>
        <ul
          style={{
            padding: '0',

            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}
        >
          {dots}
        </ul>
      </div>
    ),

    customPaging: i => (
      <div
        className="dots-custom"
        style={{
          fontSize: '0',
          borderRadius: '22px',
          borderTop: '6px solid white',
        }}
      >
        {i + 1}{' '}
      </div>
    ),
  };

  return (
    <div className="wrapperCarousel">
      <div className="containerCarousel">
        <Slider {...settings}>
          <div className="sliderPromo">
            <div className="sliderText">
              <h1> Promo </h1>
              <p>Marketing promo banner</p>
            </div>
            <img src="https://picsum.photos/536/354" alt="" />
          </div>
          <div className="sliderPromo">
            <div className="sliderText">
              <h1> Promo </h1>
              <p>Marketing promo banner</p>
            </div>
            <img src="https://picsum.photos/534/354" alt="" />
          </div>
          <div className="sliderPromo">
            <div className="sliderText">
              <h1> Promo </h1>
              <p>Marketing promo banner</p>
            </div>
            <img src="https://picsum.photos/537/354" alt="" />
          </div>
          <div className="sliderPromo">
            <div className="sliderText">
              <h1> Promo </h1>
              <p>Marketing promo banner</p>
            </div>
            <img src="https://picsum.photos/531/354" alt="" />
          </div>
        </Slider>
      </div>
    </div>
  );
};

export default Carousel;
