import ItemVBoard from './../../../components/ItemVBoard';
import ReactDOM from 'react-dom';
import { useState } from 'react';
import ModalAllVBoard from '../../../components/ItemVBoard/ModalAllVBoard';

import './style.scss';

const VBoards = ({ videos, qty = 2, userPager }) => {
  const [toggleModalAll, setToggleModalAll] = useState(false);

  return (
    <>
      <div className="wrapperVBoards">
        <div className="containerVBoards">
          {videos.slice(0, qty).map(item => (
            <div key={item._id} className="wrapperItemVBoard">
              {' '}
              <ItemVBoard item={item} />{' '}
            </div>
          ))}
          <div
            className="MainBtnMore"
            onClick={() => setToggleModalAll(prev => !prev)}
          >
            More
          </div>
        </div>
      </div>
      {toggleModalAll &&
        ReactDOM.createPortal(
          <ModalAllVBoard
            qty={videos.length}
            userPager={userPager}
            userVBoard={videos}
            toggleModalAll={toggleModalAll}
            setToggleModalAll={setToggleModalAll}
          />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default VBoards;
