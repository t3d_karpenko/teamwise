import OpenAll from '../../../components/OpenAll';
import ItemProfessional from '../../../components/ItemProfessional';
import './style.scss';

const Professionals = ({ title, professionals, qty = 3 }) => {
  const qtyall = professionals?.length;

  return (
    <div className="wrapperProfessionals">
      <div className="containerProfessionals">
        <OpenAll title={title} qtyall={qtyall} />
        {professionals.slice(0, qty).map(item => (
          <div key={item._id} className="wrapperItemProfessional">
            <ItemProfessional item={item} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Professionals;
