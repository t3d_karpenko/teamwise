import InfoBlockProfileUser from './InfoBlockProfileUser';
import DescriptionProfileUser from './DescriptionProfileUser';
import AchievementsProfileUser from './AchievementsProfileUser';
import UserAvatarImg from '../../../assets/userAvatar.png';

import './style.scss';

const ProfileUser = ({ user }) => {
  return (
    <div className="wrapperProfileUser">
      <div className="containerProfileUser">
        <div className="dataProfileUser">
          <img
            className="avatarProfileUser"
            src={user.avatar || UserAvatarImg}
            alt=""
          />
          <div className="nameProfileUser">
            {user.firstName} {user.lastName}
          </div>
          <div className=" categoryProfileUser">{user.category}</div>
          <div className="addressProfileUser">
            {user?.address?.city}, {user?.address?.country}
          </div>
        </div>
        <AchievementsProfileUser user={user} />
        <InfoBlockProfileUser user={user} />
        {user?.advanced && <DescriptionProfileUser user={user} />}
      </div>
    </div>
  );
};

export default ProfileUser;
