import './style.scss';
import { useState } from 'react';

const DescriptionProfileUser = ({ user }) => {
  const [btnDescriptionProfileUser, setBtnDescriptionProfileUser] =
    useState(true);

  return (
    <>
      <div
        className={
          btnDescriptionProfileUser
            ? 'overlayProfileUser '
            : 'overlayProfileUser active'
        }
        onClick={() => setBtnDescriptionProfileUser(prev => !prev)}
      />

      <div className="containerDescriptionProfileUser">
        {btnDescriptionProfileUser ? (
          <div className="descriptionProfileUser">
            <div className="textDescriptionProfileUser">
              {user?.advanced?.Description && (
                <>
                  <div className="titleDescriptionProfileUser">Description</div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.Description}
                  </div>
                </>
              )}
            </div>
            <div
              className="btnDescriptionProfileUser"
              onClick={() => setBtnDescriptionProfileUser(prev => !prev)}
            >
              {btnDescriptionProfileUser && 'All'}
            </div>
          </div>
        ) : (
          <div className="descriptionProfileUser active">
            <div className="textDescriptionProfileUser">
              {user?.advanced?.Description && (
                <>
                  <div className="titleDescriptionProfileUser">Description</div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.['Main text']}
                  </div>
                </>
              )}

              {!!user?.advanced?.Advantages.length && (
                <>
                  <div className="titleDescriptionProfileUser">Advantages</div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.Advantages.map((item, index) => (
                      <li key={index}>{item}</li>
                    ))}
                  </div>
                </>
              )}

              {!!user?.advanced?.['Credentials and affiliations'].length && (
                <>
                  <div className="titleDescriptionProfileUser">
                    Credentials and affiliations
                  </div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.['Credentials and affiliations'].map(
                      (item, index) => (
                        <li key={index}>{item}</li>
                      ),
                    )}
                  </div>
                </>
              )}

              {user?.advanced?.Website && (
                <>
                  <div className="titleDescriptionProfileUser">Website</div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.Website}
                  </div>
                </>
              )}

              {user?.advanced?.['Years of experience'] && (
                <>
                  <div className="titleDescriptionProfileUser">
                    Years of experience
                  </div>
                  <div className="bodyDescriptionProfileUser">
                    {user?.advanced?.['Years of experience']}
                  </div>
                </>
              )}
            </div>
            <div
              className="btnDescriptionProfileUser"
              onClick={() => setBtnDescriptionProfileUser(prev => !prev)}
            >
              {!btnDescriptionProfileUser && 'Close'}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DescriptionProfileUser;
