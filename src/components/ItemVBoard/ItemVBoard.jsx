import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import ReactPlayer from 'react-player';
import ItemStatistics from '../ItemStatistics';
import { ReactComponent as ImgClose } from '../../assets/close.svg';
import { ReactComponent as UserImg } from '../../assets/user.svg';

import './style.scss';
import axios from 'axios';

const ItemBoard = ({ item }) => {
  const [thumb, setThumb] = useState('');
  const history = useHistory();
  const [toggleModal, setToggleModal] = useState(true);

  useEffect(() => {
    axios
      .get(`https://vimeo.com/api/oembed.json?url=${item.video}`)
      .then(data => {
        setThumb(data.data.thumbnail_url);
      });
  }, [item]);

  return (
    <div className="containerItemVBoard">
      {toggleModal ? (
        <>
          {' '}
          <div className="bannerItemVBoard">
            <div className="headerBannerItemVBoard">
              <div className="titleBannerItemVBoard">Video Board</div>
              <div className="descriptionBannerItemVBoard">{item?.title}</div>
            </div>

            <img className="coverBannerItemVBoard" src={thumb} alt="" />
            <div className="authorItemVBoard">
              <div
                className="authorNameItemVBoard"
                onClick={() => {
                  history.push(`/profile/${item.owner.link}`);
                }}
              >
                {item?.owner?.firstName} {item?.owner?.lastName}
              </div>
              {item?.owner?.avatar ? (
                <img
                  className="authorAvatarItemVBoard"
                  src={item.owner.avatar}
                  alt=""
                  onClick={() => {
                    history.push(`/profile/${item.owner.link}`);
                  }}
                />
              ) : (
                <UserImg />
              )}
            </div>
          </div>
          <div className="infoItemVBoard">
            <div
              className="buttonItemVBoard"
              onClick={() => setToggleModal(prev => !prev)}
            >
              Watch
            </div>
            <div className="statisticsItemVBoard">
              <ItemStatistics item={item} />
            </div>
          </div>{' '}
        </>
      ) : (
        <div className="videoPlayerItemVBoard">
          <div
            className="btnCloseVideoPlayerItemVBoard"
            onClick={() => setToggleModal(prev => !prev)}
          >
            {' '}
            <ImgClose />{' '}
          </div>
          <ReactPlayer
            className="videoPlayer"
            controls={true}
            url={item?.video}
          />
        </div>
      )}
    </div>
  );
};

export default ItemBoard;
