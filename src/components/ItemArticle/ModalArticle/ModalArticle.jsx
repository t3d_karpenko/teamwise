import Slider from 'react-slick';
import 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import ItemStatistics from '../../ItemStatistics';
import { ReactComponent as ImgClose } from '../../../assets/close.svg';
import './style.scss';

const ModalArticle = ({ setToggleModal, item }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    pauseOnHover: true,
    arrows: false,
    dotsClass: 'slickDotsModalArticle',
    className: 'sliderSlictModalArticle',

    appendDots: dots => (
      <div style={{}}>
        <ul
          style={{
            padding: '0',

            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}
        >
          {dots}
        </ul>
      </div>
    ),

    customPaging: i => (
      <div
        className="dots-customModalArticle"
        style={{
          fontSize: '0',
          borderRadius: '22px',
          borderTop: '6px solid white',
        }}
      >
        {i + 1}{' '}
      </div>
    ),
  };

  return (
    <div className="wrapperModalArticle">
      <div className="containerModalArticle">
        <div className="titleModalArticle">{item.title}</div>

        <div className="wrapperParagraphModalArticle">
          {item?.paragraph.map((paragraph, index) => (
            <div key={index} className="paragraphModalArticle">
              <div className="wrapperSliderModalArticle">
                <Slider {...settings}>
                  {paragraph.images.map((images, index) => (
                    <img
                      key={index}
                      className="imgSliderModalArticle"
                      src={images.url}
                      alt=""
                    />
                  ))}
                </Slider>
              </div>
              <div className="paragraphTextModalArticle">{paragraph.text}</div>
            </div>
          ))}
        </div>

        <div className="infoModalArticle">
          <div className="authorModalArticle">
            <img
              className="authorAvatarModalArticle"
              src={item?.owner?.avatar}
              alt=""
            />
            <div className="authorNameModalArticle">
              {item?.owner?.firstName} {item?.owner?.lastName}
            </div>
          </div>

          <div className="statisticsModalArticle">
            <ItemStatistics item={item} />
          </div>
        </div>

        <div
          className="closeModalArticle"
          onClick={() => setToggleModal(prev => !prev)}
        >
          {' '}
          <ImgClose />{' '}
        </div>
      </div>
    </div>
  );
};

export default ModalArticle;
