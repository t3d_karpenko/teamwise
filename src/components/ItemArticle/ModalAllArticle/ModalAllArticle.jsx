import { useEffect, useState } from 'react';

import axios from './../../../utils/axios';
import { url } from './../../../config';
import { ReactComponent as IconSortDropdown } from '../../../assets/sortDropdown.svg';

import LoadingScreen from '../../LoadingSreen/LoadingSreen';
import ItemArticle from '../ItemArticle';
import './style.scss';

const ModalAllArticle = ({
  userArticles,
  setToggleModalAll,
  qty,
  userPager,
}) => {
  const [tabDropdownSort, setTabDropdownSort] = useState(false);
  const [tabCheckmarker, setTabCheckmarker] = useState(false);

  const [articles, setArticles] = useState(null);

  useEffect(() => {
    console.log(userArticles, userPager);
    if (userPager) {
      setArticles(userArticles);
      return;
    }

    axios
      .get(`${url}/api/media?type=article`)
      .then(res => {
        setArticles(res.data.body);
      })
      .catch(err => {
        console.log(err);
      });
  }, [userArticles, userPager]);

  if (!articles) return <LoadingScreen />;

  qty = articles.length;

  return (
    <div className="wrapperModalAll">
      <div className="containerModalAll">
        <div className="containerArticles">
          {(tabCheckmarker
            ? articles.slice(0, qty).reverse()
            : articles.slice(0, qty)
          ).map(item => (
            <div key={item._id} className="wrapperItemArticle">
              <ItemArticle item={item} />
            </div>
          ))}
        </div>
        <div className="sortingBlockModalAll">
          <div
            className="headerSortingBlockModalAll"
            onClick={() => setTabDropdownSort(prev => !prev)}
          >
            <div className="textSortingBlockModalAll">Sort by </div>
            <div
              className={
                tabDropdownSort
                  ? 'iconDropdownSortingBlockModalAll active'
                  : 'iconDropdownSortingBlockModalAll'
              }
            >
              <IconSortDropdown />
            </div>
          </div>

          <div
            className={
              tabDropdownSort
                ? 'dropdownSortingBlockModalAll '
                : 'dropdownSortingBlockModalAll active'
            }
          >
            <div
              className="itemDropdownSortingBlockModalAll"
              onClick={() => setTabCheckmarker(false)}
            >
              <label className="checkboxItemModalAll">
                From new to old
                <span
                  className={
                    tabCheckmarker
                      ? 'checkmarkModalAll '
                      : 'checkmarkModalAll active'
                  }
                ></span>
              </label>
            </div>

            <div
              className="itemDropdownSortingBlockModalAll"
              onClick={() => setTabCheckmarker(true)}
            >
              <label className="checkboxItemModalAll">
                From old to new
                <span
                  className={
                    tabCheckmarker
                      ? 'checkmarkModalAll active'
                      : 'checkmarkModalAll'
                  }
                ></span>
              </label>
            </div>
          </div>
        </div>

        <div
          className="bntCloseModalAll"
          onClick={() => setToggleModalAll(prev => !prev)}
        >
          {' '}
          Close
        </div>
      </div>
    </div>
  );
};

export default ModalAllArticle;
