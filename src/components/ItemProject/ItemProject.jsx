import './style.scss';
import { useHistory } from 'react-router-dom';
import { ReactComponent as LikesImg } from '../../assets/likes.svg';
import { ReactComponent as FavoritesImg } from '../../assets/favorites.svg';
import { ReactComponent as UserImg } from '../../assets/user.svg';

const ItemProject = ({ item }) => {
  const history = useHistory();

  return (
    <div className="containerItemProject">
      <div className="headerItemProject">
        <img className="coverItemProject" src={item?.file2d[0]?.url} alt="" />
        <div className="authorItemProject">
          {item?.owner?.avatar ? (
            <img
              className="authorAvatarItemProject"
              src={item.owner.avatar}
              alt=""
              onClick={() => {
                history.push(`/profile/${item.owner.link}`);
              }}
            />
          ) : (
            <UserImg />
          )}
          <div
            className="authorNameItemProject"
            onClick={() => {
              history.push(`/profile/${item.owner.link}`);
            }}
          >
            {item?.owner?.firstName} {item?.owner?.lastName}
          </div>
        </div>
        <div className="navItemProject">
          <div className="navLikesItemProject">
            <LikesImg />
          </div>
          <div className="navFavoritesItemProject">
            <FavoritesImg />
          </div>
        </div>
      </div>
      <div className="titleItemProject">{item.title}</div>
      <div className="categoryItemProject">{item.category}</div>
      <div className="addressItemProject">
        {item.location.city}, {item.location.country}
      </div>
      <div className="priceItemProject">
        {item.cost} {item.currency}
      </div>
    </div>
  );
};
export default ItemProject;
