import { useEffect, useState } from 'react';

import axios from './../../../utils/axios';
import { url } from './../../../config';
import { ReactComponent as IconSortDropdown } from '../../../assets/sortDropdown.svg';

import LoadingScreen from '../../LoadingSreen/LoadingSreen';
import ItemPodcast from '../ItemPodcast';
import './style.scss';

const ModalAllPodcast = ({
  userPodcast,
  setToggleModalAll,
  qty,
  userPager,
}) => {
  const [tabDropdownSort, setTabDropdownSort] = useState(false);
  const [tabCheckmarker, setTabCheckmarker] = useState(false);

  const [podcast, setPodcast] = useState(null);

  useEffect(() => {
    if (userPager) {
      setPodcast(userPodcast);
      return;
    }

    axios
      .get(`${url}/api/media?type=podcast`)
      .then(res => {
        setPodcast(res.data.body);
      })
      .catch(err => {
        console.log(err);
      });
  }, [userPodcast, userPager]);

  if (!podcast) return <LoadingScreen />;

  qty = podcast.length;

  return (
    <div className="wrapperModalAll">
      <div className="containerModalAll">
        <div className="containerArticles">
          {(tabCheckmarker
            ? podcast.slice(0, qty).reverse()
            : podcast.slice(0, qty)
          ).map(item => (
            <div key={item._id} className="wrapperItemArticle">
              <ItemPodcast item={item} />
            </div>
          ))}
        </div>
        <div className="sortingBlockModalAll">
          <div
            className="headerSortingBlockModalAll"
            onClick={() => setTabDropdownSort(prev => !prev)}
          >
            <div className="textSortingBlockModalAll">Sort by </div>
            <div
              className={
                tabDropdownSort
                  ? 'iconDropdownSortingBlockModalAll active'
                  : 'iconDropdownSortingBlockModalAll'
              }
            >
              <IconSortDropdown />
            </div>
          </div>

          <div
            className={
              tabDropdownSort
                ? 'dropdownSortingBlockModalAll '
                : 'dropdownSortingBlockModalAll active'
            }
          >
            <div
              className="itemDropdownSortingBlockModalAll"
              onClick={() => setTabCheckmarker(false)}
            >
              <label className="checkboxItemModalAll">
                From new to old
                <span
                  className={
                    tabCheckmarker
                      ? 'checkmarkModalAll '
                      : 'checkmarkModalAll active'
                  }
                ></span>
              </label>
            </div>

            <div
              className="itemDropdownSortingBlockModalAll"
              onClick={() => setTabCheckmarker(true)}
            >
              <label className="checkboxItemModalAll">
                From old to new
                <span
                  className={
                    tabCheckmarker
                      ? 'checkmarkModalAll active'
                      : 'checkmarkModalAll'
                  }
                ></span>
              </label>
            </div>
          </div>
        </div>

        <div
          className="bntCloseModalAll"
          onClick={() => setToggleModalAll(prev => !prev)}
        >
          {' '}
          Close
        </div>
      </div>
    </div>
  );
};

export default ModalAllPodcast;
