import './style.scss';
import { useHistory } from 'react-router-dom';
import { ReactComponent as MessagesImg } from '../../assets/messages.svg';
import { ReactComponent as FavoritesImg } from '../../assets/favorites.svg';
import UserAvatarImg from '../../assets/userAvatar.png';

const ItemProfessional = ({ item }) => {
  const history = useHistory();

  return (
    <div className="containerItemProfessional">
      <div className="headerItemProfessional">
        <img
          className="avatarItemProfessional"
          src={item.avatar || UserAvatarImg}
          alt=""
          onClick={() => {
            history.push(`/profile/${item.link}`);
          }}
        />

        <div className="navItemProfessional">
          <div className="navMessagesItemProfessional">
            <MessagesImg />
          </div>
          {item?.type === 'Contractor' && (
            <div className="navFavoritesItemProfessional">
              <FavoritesImg />
            </div>
          )}
        </div>
      </div>
      <div className="infoItemProfessional">
        <div
          className="nameItemProfessional"
          onClick={() => {
            history.push(`/profile/${item.owner.link}`);
          }}
        >
          {item?.firstName} {item?.lastName}
        </div>
        <div className="companyItemProfessional">{item?.company || ''}</div>
        <div className="addressItemProfessional">
          {item?.address?.country}, {item?.address?.city}
        </div>
        {item?.type === 'Contractor' && (
          <div className="subscriptionItemProfessional">PRO</div>
        )}
        <div
          className={
            item?.status === 'free'
              ? 'statusItemProfessional'
              : 'statusItemProfessional active'
          }
        >
          {item?.status}
        </div>
      </div>
    </div>
  );
};
export default ItemProfessional;
