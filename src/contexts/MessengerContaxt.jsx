import { createContext, useEffect, useReducer } from 'react';
import io from 'socket.io-client';
import { url } from '../config';
import axios from '../utils/axios';
import useAuth from './../hooks/useAuth';
import AudioMessage from './../assets/soundmessages.mp3';

const initialState = {
  threads: [],
  thread: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'INITTHREADS': {
      const { threads } = action.payload;
      return {
        ...state,
        threads,
      };
    }
    case 'NEWMESSAGE': {
      const { message } = action.payload;
      const read = state.thread === message.thread;

      if (read) {
        axios.post(`${url}/api/messenger/thread/mark-as-read`, {
          threadId: state.thread,
        });
      }

      return {
        ...state,
        threads: state.threads.map(item => {
          if (item._id === message.thread) {
            return {
              ...item,
              messages: [{ ...message, isRead: read }, ...item.messages],
            };
          } else {
            return item;
          }
        }),
      };
    }

    case 'NEWTHREAD': {
      const { thread } = action.payload;
      return {
        ...state,
        threads: [thread, ...state.threads],
      };
    }

    case 'DELETETHREAD': {
      const { thread } = action.payload;
      console.log(state);
      return {
        ...state,
        threads: state.threads.filter(item => item._id !== thread._id),
      };
    }

    case 'READMESSAGES': {
      const { threadId, user } = action.payload;
      console.log(state, threadId);
      return {
        ...state,
        threads: state.threads.map(item => {
          if (item._id === threadId) {
            return {
              ...item,
              messages: item.messages.map(itemmessage => {
                if (itemmessage.sender._id !== user._id) {
                  return {
                    ...itemmessage,
                    isRead: true,
                  };
                } else {
                  return itemmessage;
                }
              }),
            };
          } else {
            return item;
          }
        }),
      };
    }

    case 'SETTHREAD': {
      const { thread } = action.payload;

      return {
        ...state,
        thread: thread,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
};

const MessengerContext = createContext({
  ...initialState,
  readMessages: () => {},
  setThread: () => {},
});

export const MessengerProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { user } = useAuth();

  useEffect(() => {
    let socket = null;
    const soundMessage = new Audio(AudioMessage);

    const threads = async () => {
      if (!user) return;
      const res = await axios.get(`${url}/api/messenger/threads`);

      dispatch({
        type: 'INITTHREADS',
        payload: {
          threads: res.data.body,
        },
      });

      socket = io(url, {
        transports: ['websocket', 'polling', 'flashsocket'],
        query: `user=${user._id}`,
      });

      socket.on('connect', () => {
        console.log('socket connect');
      });

      socket.on('disconnect', reason => {
        if (reason === 'io server disconnect') {
          socket.connect();
        }
      });

      socket.on('message', data => {
        if (window.location.pathname !== '/messenger') {
          soundMessage.play();
        }

        dispatch({
          type: 'NEWMESSAGE',
          payload: {
            message: data,
          },
        });
      });

      socket.on('thread', data => {
        dispatch({
          type: 'NEWTHREAD',
          payload: {
            thread: data,
          },
        });
      });

      socket.on('delete-thread', data => {
        dispatch({
          type: 'DELETETHREAD',
          payload: {
            thread: data,
          },
        });
      });
    };
    threads();
    return () => {
      if (socket) {
        socket.disconnect();
      }
    };
  }, [user]);

  const readMessages = async threadId => {
    await axios
      .post(`${url}/api/messenger/thread/mark-as-read`, { threadId: threadId })
      .catch(err => {
        console.log(err);
      });
    dispatch({
      type: 'READMESSAGES',
      payload: { threadId, user },
    });
  };

  const setThread = thread => {
    dispatch({
      type: 'SETTHREAD',
      payload: { thread },
    });
  };

  return (
    <MessengerContext.Provider value={{ ...state, readMessages, setThread }}>
      {children}
    </MessengerContext.Provider>
  );
};

export default MessengerContext;
